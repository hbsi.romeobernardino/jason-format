chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    if (message.action === 'triggerContentScript') {
      // Send a message to the content script
      chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        var activeTab = tabs[0];
        chrome.tabs.sendMessage(activeTab.id, { message: 'ContentScriptTriggered' });
      });
    }
  });