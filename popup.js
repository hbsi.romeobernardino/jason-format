const myInput = document.getElementById('myInput');

chrome.storage.sync.get(['myString'], function(result) {
    const loadedString = result.myString;
    myInput.value = loadedString
})

// Initialize a variable to store the input value
let inputValue = '';

// Set a delay (in milliseconds) for the input change
const delay = 500; // 500 milliseconds (0.5 seconds)

// Initialize a timer variable
let timer;

// Add an event listener to the input field for the "input" event
myInput.addEventListener('input', function(event) {
  // Clear the previous timer (if any)
  clearTimeout(timer);

  // Set a new timer to update the variable after the delay
  timer = setTimeout(() => {
    chrome.runtime.sendMessage({ action: 'triggerContentScript' });
    // Update the variable with the current input value
    inputValue = event.target.value;

    // You can perform any further actions here based on the updated value
    console.log('Input value after delay:', inputValue);

    chrome.storage.sync.set({ myString: inputValue }, function() {
        console.log('Value is set.');
    });
  }, delay);
});