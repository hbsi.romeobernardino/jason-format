let applyTheme = () => {
    let theme = 'html {';

    chrome.storage.sync.get(['myString'], function(result) {
        const loadedString = result.myString;

        theme += loadedString;
        theme += '}';

        const cssStyle2 = '.el-card { border-radius: 1rem; padding: 1rem; box-shadow: none !important; outline-width: thin; outline-style: dashed; }';
        const cssStyle3 = 'body { font-family: arial, impact !important}';
        const combinedStyles = theme + cssStyle2 + cssStyle3;
        const styleElement = document.createElement('style');
        styleElement.textContent = combinedStyles;
        document.head.appendChild(styleElement);
    })
}
applyTheme();

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    applyTheme();
});